import { makeStyles, Theme } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme: Theme) => ({
  icon: {
    color: theme.palette.secondary.light,
    "& :visited": { color: "red" },
    "& :hover": { color: "red" },
    "& :active": { color: "red" },
    border: theme.spacing(0),
    backgroundSize: "80px",
    boxSize: "80px",
    marginTop: "15px",
    fontSize: "35px",
  },
  loginIcon: {
    color: theme.palette.secondary.light,
    fontSize: "35px",
  },
  registerForm: {
    margin: theme.spacing(1, 2),
    textAlign: "center",
  },
  indexContainer: {
    paddingTop: theme.spacing(2),
  },
  cardModal: {
    paddingTop: theme.spacing(4),
    marginBottom: "35px",
    borderWidth: "1px",
    borderRadius: "20px",
    textAlign: "center",
    boxShadow: "lg",
    // backgroundColor: theme.palette.primary.light
  },
}));
