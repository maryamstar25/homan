import { Box } from "@chakra-ui/layout";
import { Container } from "@chakra-ui/react";
import React from "react";
import { useStyles } from "../styles/styles";

export type WrapperVariant = "small" | "regular";

interface WrapperProps {
  variant?: WrapperVariant;
}

export const Wrapper: React.FC<WrapperProps> = ({ children }) => {
  const classes = useStyles();

  return (
    <Container>
      <Box className={classes.cardModal}>
        <Container>
          <Box m="5">{children}</Box>
        </Container>
      </Box>
    </Container>
  );
};
