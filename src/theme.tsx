import { extendTheme } from '@chakra-ui/react'
import { createBreakpoints } from '@chakra-ui/theme-tools'
import { createMuiTheme } from '@material-ui/core';

const fonts = { mono: `'Menlo', monospace` }

const breakpoints = createBreakpoints({
  sm: '40em',
  md: '52em',
  lg: '64em',
  xl: '80em',
})

export const muiTheme = createMuiTheme({
  direction: 'rtl',
});

const theme = extendTheme({
  // direction: 'rtl',
  colors: {
    black: '#16161D',
  },
  fonts,
  breakpoints,

})

export default theme
